package kvstore

import akka.actor.{ OneForOneStrategy, Props, ActorRef, Actor }
import kvstore.Arbiter._
import scala.collection.immutable.Queue
import akka.actor.SupervisorStrategy.Restart
import scala.annotation.tailrec
import akka.pattern.{ ask, pipe }
import akka.actor.Terminated
import scala.concurrent.duration._
import akka.actor.PoisonPill
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy
import akka.util.Timeout

object Replica {
  sealed trait Operation {
    def key: String
    def id: Long
  }
  case class Insert(key: String, value: String, id: Long) extends Operation
  case class Remove(key: String, id: Long) extends Operation
  case class Get(key: String, id: Long) extends Operation

  sealed trait OperationReply
  case class OperationAck(id: Long) extends OperationReply
  case class OperationFailed(id: Long) extends OperationReply
  case class GetResult(key: String, valueOption: Option[String], id: Long) extends OperationReply

  def props(arbiter: ActorRef, persistenceProps: Props): Props = Props(new Replica(arbiter, persistenceProps))
}

class Replica(val arbiter: ActorRef, persistenceProps: Props) extends Actor {
  import Replica._
  import Replicator._
  import Persistence._
  import PrimaryReplicator._
  import context.dispatcher

  //just created, let's send a message to arbiter
  arbiter ! Join
  
  var kv = Map.empty[String, String]

  def receive = {
    case JoinedPrimary   => context.become(leader)
    case JoinedSecondary => context.become(replica)
  }

  val persistence = context.actorOf(persistenceProps, "persistenceActor")

  // a map from replicators to replicas
  var secondaries = Map.empty[ActorRef, ActorRef]

  var persistenceAcks = Map.empty[Long, (ActorRef, Operation, Int)]

  var replicationStatus = Map.empty[Long, Option[ActorRef]]
  
  var replicates = Set.empty[Replicate]
  
  var primaryReps = Set.empty[ActorRef]
  
  val leader: Receive = {
    case Replicas(replicas) => {
      //remove replicas no longer present
      val removed = secondaries.filter(s => !replicas.tail.contains(s._2)) 
      removed.foreach{ s =>
        s._1 ! PoisonPill
        secondaries -= s._1
      }
      
      primaryReps.foreach(p => p ! KillSecondary(removed.map(_._1).toSet))
      
      //insert all new replicas
      val newSecondaries = (replicas.tail -- secondaries.values).map { r => 
      	val newReplicator = context.actorOf(Replicator.props(r))
        newReplicator -> r
      }

      replicates foreach { r =>
        primaryReps += context.actorOf(PrimaryReplicator.props(this.self, newSecondaries.map(_._1), r))
      }
      
      newSecondaries.foreach(r => secondaries += r)
    }
    case Insert(key, value, id) => {
      kv += key -> value
      
      val replicate = Replicate(key, Some(value), id)
      if(!secondaries.isEmpty){
        primaryReps += context.actorOf(PrimaryReplicator.props(this.self, secondaries.keys.toSet, replicate))
        replicationStatus += id -> Some(sender)        
      }
      replicates += replicate
      
      persistenceAcks += id -> (sender,  Insert(key, value, id), 0)
      persistence ! Persist(key, Some(value), id)
    }
    case Remove(key, id) => {
      kv -= key

      val replicate = Replicate(key, None, id)
      if(!secondaries.isEmpty){
        primaryReps += context.actorOf(PrimaryReplicator.props(this.self, secondaries.keys.toSet, replicate))
        replicationStatus += id -> Some(sender)        
      }
      replicates += replicate
      
      persistenceAcks += id -> (sender,  Remove(key, id), 0)
      persistence ! Persist(key, None, id)
    }
    case Get(key, id) => sender ! GetResult(key, kv.get(key), id)
    case Persisted(key, id) => {
      val operationAck = persistenceAcks.get(id).get 
      persistenceAcks -= id
      if(!replicationStatus.contains(id)){
      	operationAck._1 ! OperationAck(id)        
      }
    }
    case Replicated(key, id) => {
      sender ! PoisonPill
      primaryReps -= sender
      println(id)
      println(persistenceAcks)
      println(replicationStatus)
      if(!persistenceAcks.contains(id)){
        for{
          actorOpt <- replicationStatus.get(id)
          actor <- actorOpt
        } yield {
          println(id)
          println(actor)
          actor ! OperationAck(id) 
        }
      }
      replicationStatus -= id
    }
    case ReplicateFailed(id) => {
      sender ! PoisonPill
      primaryReps -= sender
      for{
        actorOpt <- replicationStatus.get(id)
        actor <- actorOpt
      } yield actor ! OperationFailed(id) 
      replicationStatus -= id
    } 
  }

  context.system.scheduler.schedule(0 milliseconds, 100 milliseconds){
    val (pending, failed) = persistenceAcks.partition(_._2._3 <= 10)
    
    pending.foreach{ ack =>
      persistenceAcks += ack._1 -> (ack._2._1, ack._2._2, ack._2._3 + 1)
      val value = ack._2._2 match {
        case Insert(key, value, id) => Some(value)
        case _ => None
      }
      persistence ! Persist(ack._2._2.key, value, ack._2._2.id)
    }
    
    failed.foreach{ ack =>
      ack._2._1 ! OperationFailed(ack._2._2.id)
      
      //cleanup replicators
    }
  }

  var expectedSeqNo = 0

  var snapshotAcks = Map.empty[String, (ActorRef, SnapshotAck, Int)]
  
  /* TODO Behavior for the replica role. */
  val replica: Receive = {
    case Get(key, id) => sender ! GetResult(key, kv.get(key), id)
    case Snapshot(key, valueOption, seq) => {
      if(seq < expectedSeqNo){
      	snapshotAcks += key -> (sender, SnapshotAck(key, seq), 0)
        sender ! SnapshotAck(key, seq)
      }else if(seq == expectedSeqNo){
	    valueOption.map(value => kv += key -> value).getOrElse(kv -= key)
	    expectedSeqNo += 1  
	    
	    snapshotAcks += key -> (sender, SnapshotAck(key, seq), 0)
	    persistence ! Persist(key, valueOption, seq)
      }
    }
    case Persisted(key, id) => {
      val snapshotAck = snapshotAcks.get(key).get 
      snapshotAcks -= key
      snapshotAck._1 ! snapshotAck._2
    }
  }
  
  context.system.scheduler.schedule(0 milliseconds, 100 milliseconds){
    snapshotAcks = snapshotAcks.filter(_._2._3 <= 10)
    
    snapshotAcks.foreach{ ack =>
      snapshotAcks += ack._1 -> (ack._2._1, ack._2._2, ack._2._3 + 1)
      persistence ! Persist(ack._1, kv.get(ack._1), ack._2._2.seq)
    }
  }
  
  override val supervisorStrategy = OneForOneStrategy(){
    case _: PersistenceException => {
      println(persistence)
      Restart
    }
  }
  
    // the current set of replicators
//  var replicators = Set.empty[ActorRef]
}