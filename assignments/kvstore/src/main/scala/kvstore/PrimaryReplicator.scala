package kvstore

import akka.actor.{Props, Actor, ActorRef}
import scala.util.Random
import java.util.concurrent.atomic.AtomicInteger
import scala.concurrent.duration._
import Replicator._
import akka.pattern.{ ask, pipe }

object PrimaryReplicator{
  case class ReplicateFailed(id: Long)
  
  case class KillSecondary(killed: Set[ActorRef]) 
  
  def props(primary: ActorRef, replicators: Set[ActorRef], replicate: Replicate): Props = Props(classOf[PrimaryReplicator], primary, replicators, replicate)
}

class PrimaryReplicator(val primary: ActorRef, var replicators: Set[ActorRef], val replicate: Replicate) extends Actor {
  import PrimaryReplicator._
  import context.dispatcher
  
  var pending = Map.empty[ActorRef, Int]

  replicators.foreach{ r =>
  	pending += r -> 0
  	r ! replicate
  }
  
  def receive = {
    case Replicated(key, id) => {
      pending -= sender
      if(pending.isEmpty){
        context.parent ! Replicated(key, id)
      }
    }
    case KillSecondary(removed) => {
      replicators --= removed
      pending --= removed
      if(pending.isEmpty){
        context.parent ! Replicated(replicate.key, replicate.id)
      }
    }
  }
  
  context.system.scheduler.schedule(100 milliseconds, 100 milliseconds){
    if(!pending.filter(kv => kv._2 == 10).isEmpty && context != null){
    	context.parent ! ReplicateFailed(replicate.id)
    }else{
      pending.foreach{ kv =>
        pending += kv._1 -> (kv._2 + 1)
        kv._1 ! replicate
      }  
    }
  }
}
