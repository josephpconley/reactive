import suggestions.gui._
import scala.language.postfixOps
import scala.collection.mutable.ListBuffer
import scala.collection.JavaConverters._
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Try, Success, Failure }
import rx.subscriptions.CompositeSubscription
import rx.lang.scala.Observable

object observables {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(449); 
  println("Welcome to the Scala worksheet")
  import rx.lang.scala.subjects._
  import rx.lang.scala.Observable
  import scala.concurrent.duration._;$skip(144); 

  val channel = ReplaySubject[Int]();System.out.println("""channel  : rx.lang.scala.subjects.ReplaySubject[Int] = """ + $show(channel ));$skip(50); 
	val a = channel.subscribe(x => println("a: "+x));System.out.println("""a  : rx.lang.scala.Subscription = """ + $show(a ));$skip(50); 
	val b = channel.subscribe(x => println("b: "+x));System.out.println("""b  : rx.lang.scala.Subscription = """ + $show(b ));$skip(23); 
	
	channel.onNext(42);$skip(17); 
	a.unsubscribe();$skip(22); 
	channel.onNext(4711);$skip(23); 
	channel.onCompleted();$skip(50); 
	val c = channel.subscribe(x => println("c: "+x));System.out.println("""c  : rx.lang.scala.Subscription = """ + $show(c ));$skip(20); 
	channel.onNext(13)
	
	import mockApi._;$skip(56); 
	
	val nums = Observable(1, 2, 3);System.out.println("""nums  : rx.lang.scala.Observable[Int] = """ + $show(nums ));$skip(58); 
	val numList = nums.recovered.toBlockingObservable.toList;System.out.println("""numList  : List[scala.util.Try[Int]] = """ + $show(numList ));$skip(76); 
	
	val example: Observable[Int] = nums ++ Observable(new Exception("oops"));System.out.println("""example  : rx.lang.scala.Observable[Int] = """ + $show(example ));$skip(59); 
  val list = example.recovered.toBlockingObservable.toList;System.out.println("""list  : List[scala.util.Try[Int]] = """ + $show(list ));$skip(34); 
  
  val to = example.timedOut(2);System.out.println("""to  : rx.lang.scala.Observable[Int] = """ + $show(to ))}
	//val ticks = Observable.interval(1 second).timedOut(5).toBlockingObservable.toList
	//example.timedOut(2).toBlockingObservable.toList
}

object mockApi extends WikipediaApi {
    def wikipediaSuggestion(term: String) = Future {
      if (term.head.isLetter) {
        for (suffix <- List(" (Computer Scientist)", " (Footballer)")) yield term + suffix
      } else {
        List(term)
      }
    }
    def wikipediaPage(term: String) = Future {
      "Title: " + term
    }
}
