import suggestions.gui._
import scala.language.postfixOps
import scala.collection.mutable.ListBuffer
import scala.collection.JavaConverters._
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{ Try, Success, Failure }
import rx.subscriptions.CompositeSubscription
import rx.lang.scala.Observable

object observables {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  import rx.lang.scala.subjects._
  import rx.lang.scala.Observable
  import scala.concurrent.duration._

  val channel = ReplaySubject[Int]()              //> channel  : rx.lang.scala.subjects.ReplaySubject[Int] = rx.lang.scala.subject
                                                  //| s.ReplaySubject@73790581
	val a = channel.subscribe(x => println("a: "+x))
                                                  //> a  : rx.lang.scala.Subscription = rx.lang.scala.subscriptions.Subscription$$
                                                  //| anon$1@33746634
	val b = channel.subscribe(x => println("b: "+x))
                                                  //> b  : rx.lang.scala.Subscription = rx.lang.scala.subscriptions.Subscription$$
                                                  //| anon$1@840ba54
	
	channel.onNext(42)                        //> a: 42
                                                  //| b: 42
	a.unsubscribe()
	channel.onNext(4711)                      //> b: 4711
	channel.onCompleted()
	val c = channel.subscribe(x => println("c: "+x))
                                                  //> c: 42
                                                  //| c: 4711
                                                  //| c  : rx.lang.scala.Subscription = rx.lang.scala.subscriptions.Subscription$$
                                                  //| anon$1@29f10c35
	channel.onNext(13)
	
	import mockApi._
	
	val nums = Observable(1, 2, 3)            //> nums  : rx.lang.scala.Observable[Int] = rx.lang.scala.Observable$$anon$9@5e8
                                                  //| caa63
	//val numList = nums.recovered.toBlockingObservable.toList
	val example: Observable[Int] = nums ++ Observable(new Exception("oops"))
                                                  //> example  : rx.lang.scala.Observable[Int] = rx.lang.scala.Observable$$anon$9
                                                  //| @8554f0c
  //val list = example.recovered.toBlockingObservable.toList
  
	//val ticks = Observable.interval(1 second).timedOut(5).toBlockingObservable.toList
	example.recovered.timedOut(1).toBlockingObservable.toList
                                                  //> res0: List[scala.util.Try[Int]] = List(Success(1), Success(2), Success(3), 
                                                  //| Failure(java.lang.Exception: oops))
                                                  
	Observable(1, 2, 3).concatRecovered(num => Observable(num, num, num)).toBlockingObservable.toList
                                                  //> res1: List[scala.util.Try[Int]] = List(Success(1), Success(1), Success(1), 
                                                  //| Success(2), Success(2), Success(2), Success(3), Success(3), Success(3))
}

object mockApi extends WikipediaApi {
	def wikipediaSuggestion(term: String) = Future {
	  if (term.head.isLetter) {
	    for (suffix <- List(" (Computer Scientist)", " (Footballer)")) yield term + suffix
	  } else {
	    List(term)
	  }
	}
	
	def wikipediaPage(term: String) = Future {
	  "Title: " + term
	}
}