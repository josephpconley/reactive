package quickcheck

object test{;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(77); 
  println("Welcome to the Scala worksheet");$skip(46); 
  val h1 = TestHeap.insert(1, TestHeap.empty);System.out.println("""h1  : quickcheck.TestHeap.H = """ + $show(h1 ));$skip(34); 
  val h2 = TestHeap.insert(2, h1);System.out.println("""h2  : quickcheck.TestHeap.H = """ + $show(h2 ));$skip(34); 
  val h3 = TestHeap.insert(3, h2);System.out.println("""h3  : quickcheck.TestHeap.H = """ + $show(h3 ));$skip(34); 
  val h4 = TestHeap.insert(4, h3);System.out.println("""h4  : quickcheck.TestHeap.H = """ + $show(h4 ));$skip(86); 
  val h5 = TestHeap.insert(7, TestHeap.insert(6, TestHeap.insert(5, TestHeap.empty)));System.out.println("""h5  : quickcheck.TestHeap.H = """ + $show(h5 ));$skip(33); 
 	val h6 = TestHeap.meld(h4, h5);System.out.println("""h6  : quickcheck.TestHeap.H = """ + $show(h6 ));$skip(86); 
	val dec = TestHeap.insert(1, TestHeap.insert(2, TestHeap.insert(3, TestHeap.empty)));System.out.println("""dec  : quickcheck.TestHeap.H = """ + $show(dec ));$skip(80); val res$0 = 
	h3 match {
		case Nil => Nil
		case t::ts => TestHeap.meld(t.c.reverse, ts)
	};System.out.println("""res0: quickcheck.TestHeap.H = """ + $show(res$0));$skip(85); val res$1 = 
   
	dec match {
		case Nil => Nil
		case t::ts => TestHeap.meld(t.c.reverse, ts)
	};System.out.println("""res1: quickcheck.TestHeap.H = """ + $show(res$1))}
}

object TestHeap extends BinomialHeap with IntHeap{
	def getMin(t: Node, ts: H): (Node, H) = ts match {
    case Nil => (t, Nil)
    case tp::tsp =>
      val (tq,tsq) = getMin(tp, tsp)
      if (ord.lteq(root(t),root(tq))) (t,ts) else (tq,t::tsq)
  }
}
