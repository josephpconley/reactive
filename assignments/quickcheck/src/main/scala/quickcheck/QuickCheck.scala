package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("getMin") = forAll { a: Int =>
  	val b = scala.util.Random.nextInt
  	val h = insert(b, insert(a, empty))
  	findMin(h) == ord.min(a, b)
  }
  
  property("testIncreasingInserts") = forAll { a: Int => 
    //handle edge cases for a randomly generated Int
    val n = if(a > 0) a - 2 else a + 2
    val h = insert(n, insert(n-1, insert(n-2, empty)))
    findMin(h) == (n-2)
  }
  
  property("testLink") = forAll { a: Int =>
  	val b = scala.util.Random.nextInt
  	val max = ord.max(a, b)
  	val h = insert(b, insert(a, empty))
  	findMin(deleteMin(h)) == max
  }

  property("testIncreasingInsertsWithDeletion") = forAll { a: Int =>
    //handle edge cases for a randomly generated Int
    val n = if(a > 0) a - 2 else a + 2
    val h = insert(n, insert(n-1, insert(n-2, empty)))
    findMin(deleteMin(h)) == n-1
  }
  
  property("testHeap") = forAll { (h: H) =>
  	val min = findMin(h)
    val n = if(min == Int.MinValue) min else min - 1
  	deleteMin(insert(n,h)) == h
  }
  
  lazy val genHeap: Gen[H] = for {
    n <- arbitrary[Int]
    h <- oneOf(value(empty), genHeap)
  } yield insert(n, h)

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}