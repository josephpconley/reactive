package quickcheck

object test{
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val h1 = TestHeap.insert(1, TestHeap.empty)     //> h1  : quickcheck.TestHeap.H = List(Node(1,0,List()))
  val h2 = TestHeap.insert(2, h1)                 //> h2  : quickcheck.TestHeap.H = List(Node(1,1,List(Node(2,0,List()))))
  val h3 = TestHeap.insert(3, h2)                 //> h3  : quickcheck.TestHeap.H = List(Node(3,0,List()), Node(1,1,List(Node(2,0,
                                                  //| List()))))
  val h4 = TestHeap.insert(4, h3)                 //> h4  : quickcheck.TestHeap.H = List(Node(1,2,List(Node(3,1,List(Node(4,0,List
                                                  //| ()))), Node(2,0,List()))))
  val h5 = TestHeap.insert(7, TestHeap.insert(6, TestHeap.insert(5, TestHeap.empty)))
                                                  //> h5  : quickcheck.TestHeap.H = List(Node(7,0,List()), Node(5,1,List(Node(6,0,
                                                  //| List()))))
 	val h6 = TestHeap.meld(h4, h5)            //> h6  : quickcheck.TestHeap.H = List(Node(7,0,List()), Node(5,1,List(Node(6,0,
                                                  //| List()))), Node(1,2,List(Node(3,1,List(Node(4,0,List()))), Node(2,0,List()))
                                                  //| ))
	val dec = TestHeap.insert(1, TestHeap.insert(2, TestHeap.insert(3, TestHeap.empty)))
                                                  //> dec  : quickcheck.TestHeap.H = List(Node(1,0,List()), Node(2,1,List(Node(3,0
                                                  //| ,List()))))
	h3 match {
		case Nil => Nil
		case t::ts => TestHeap.meld(t.c.reverse, ts)
	}                                         //> res0: quickcheck.TestHeap.H = List(Node(1,1,List(Node(2,0,List()))))
   
	dec match {
		case Nil => Nil
		case t::ts => TestHeap.meld(t.c.reverse, ts)
	}                                         //> res1: quickcheck.TestHeap.H = List(Node(2,1,List(Node(3,0,List()))))
}

object TestHeap extends BinomialHeap with IntHeap{
	def getMin(t: Node, ts: H): (Node, H) = ts match {
    case Nil => (t, Nil)
    case tp::tsp =>
      val (tq,tsq) = getMin(tp, tsp)
      if (ord.lteq(root(t),root(tq))) (t,ts) else (tq,t::tsq)
  }
}