package simulations

import math.random
import util.Random

class EpidemySimulator extends Simulator {

  def randomBelow(i: Int) = (random * i).toInt

  def randomDays = randomBelow(5) + 1
  
  //p should be between 0 and 1
  def randomProb(p: Double): Boolean = randomBelow(100) < (p * 100) 
  
  protected[simulations] object SimConfig {
    val population: Int = 300
    val roomRows: Int = 8
    val roomColumns: Int = 8

    // to complete: additional parameters of simulation
    val prevalenceRate: Double = 0.01
    val transmissionRate: Double = 0.4
    val deathRate: Double = 0.25
    
    //infection status
    val DAYS_TO_SICK: Int = 6
    val DAYS_TO_JUDGMENT: Int = 14
    val DAYS_TO_IMMUNE: Int = 16
    val DAYS_TO_HEALTH: Int = 18
    
    //extensions (disabled by setting to 0 or false)
    val takeToAir: Double = 0
    val reducedMobilityAct: Boolean = false
    val chosenFewAct: Double = 0.05
  }

  import SimConfig._

  val persons: List[Person] = ( for( i <- List.range(0,population)) yield new Person(i))

  //infect a percentage of the population based on prevalence rate
  Random.shuffle(persons).take( (population * prevalenceRate).toInt).foreach{ p =>
  	p.infect()
  }
  
  //room functions
  def personGrid: Map[(Int, Int), List[Person]] = persons.groupBy(p => (p.row, p.col))
  
  def isVisiblyInfected(room: List[Person]) = room.exists(p => p.isVisiblyInfectious)
  
  def isInfected(room: List[Person]) = room.exists(p => p.infected)
  
  class Person (val id: Int) {
    var infected = false
    var sick = false
    var immune = false
    var dead = false
    
    // demonstrates random number generation
    var row: Int = randomBelow(roomRows)
    var col: Int = randomBelow(roomColumns)

    def isVisiblyInfectious = sick || dead
    
    //define up, down, left, and right, respectively
    def rooms = Seq( mod(row+1, roomRows) -> col, mod(row-1, roomRows) -> col, row -> mod(col-1, roomColumns), row -> mod(col+1, roomColumns) ) 
    
    def infect(){
      infected = true
      afterDelay(DAYS_TO_SICK)(makeSick)
      afterDelay(DAYS_TO_JUDGMENT)(judge)
      afterDelay(DAYS_TO_IMMUNE)(immunize)
      afterDelay(DAYS_TO_HEALTH)(heal)
    }
    
    def makeSick(){
      sick = true
    }
    
    def judge(){
      dead = randomProb(0.25)
    }
    
    def immunize(){
      if(!dead){
      	immune = true        
      }
    }

    def heal(){
    	if(!dead){
    		infected = false
    		sick = false
    		immune = false    	  
    	}
    }
    
    //move
    def move(){
    	if(!dead){
	      afterDelay(randomDays){
	      	val grid = personGrid
	        if(randomProb(takeToAir)){
	          //choose room at random
	          row = randomBelow(roomRows)
	          col = randomBelow(roomColumns)
	        }else{
		        //choose room based on neighbors
		    		val uninfectedRooms = rooms.filter(idx => grid.get(idx).map(room => !isVisiblyInfected(room)).getOrElse(false))
		    		
		    		//randomly pick a visibly healthy room.  If no visible options, then stay put!
		    		if(uninfectedRooms.size > 0){
			    		val selectedRoom = uninfectedRooms(randomBelow(uninfectedRooms.size))
			    		row = selectedRoom._1
			    		col = selectedRoom._2
		    		}	          
	        }

	    		//infect if not already infected
	      	if(!infected){
		    		infected = isInfected(grid(row -> col)) && randomProb(transmissionRate)
		    		if(infected){
		    		  infect()
		    		}  	      	  
	      	}
	    		move()
	    	}    	  
    	}
    }
  }
  
  agenda = persons.map(p => WorkItem(0, p.move))
  
  def mod(m: Int, n: Int) = if(m < 0) m + n else m % n
}
