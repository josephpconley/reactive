package simulations

import common._

class Wire {
  private var sigVal = false
  private var actions: List[Simulator#Action] = List()

  def getSignal: Boolean = sigVal
  
  def setSignal(s: Boolean) {
    if (s != sigVal) {
      sigVal = s
      actions.foreach(action => action())
    }
  }

  def addAction(a: Simulator#Action) {
    actions = a :: actions
    a()
  }
}

abstract class CircuitSimulator extends Simulator {

  val InverterDelay: Int
  val AndGateDelay: Int
  val OrGateDelay: Int

  def probe(name: String, wire: Wire) {
    wire addAction {
      () => afterDelay(0) {
        println(
          "  " + currentTime + ": " + name + " -> " +  wire.getSignal)
      }
    }
  }

  def inverter(input: Wire, output: Wire) {
    def invertAction() {
      val inputSig = input.getSignal
      afterDelay(InverterDelay) { output.setSignal(!inputSig) }
    }
    input addAction invertAction
  }

  def orGate(a1: Wire, a2: Wire, output: Wire) {
  	def orAction() = {
  	  val a1Sig = a1.getSignal
  	  val a2Sig = a2.getSignal
  	  afterDelay(OrGateDelay){ output setSignal (a1Sig | a2Sig)}
  	}
  	a1 addAction orAction
  	a2 addAction orAction
  }
  
  def orGate2(a1: Wire, a2: Wire, output: Wire) {
    val notIn1, notIn2, notOut = new Wire
    inverter(a1, notIn1)
    inverter(a2,  notIn2)
    andGate(notIn1, notIn2, notOut)
    inverter(notOut, output)
  }

  def demux(in: Wire, c: List[Wire], out: List[Wire]) {
//    def dAction() = {
//      val 
//    }
//    
    c match {
      case Nil => out.head.setSignal(in.getSignal)
      case wire::tail => wire.getSignal match {
        case false => demux(in, tail, out.drop(out.size / 2))
        case true => demux(in, tail, out.dropRight(out.size / 2))
      }
    }
  }
  
  def andGate(a1: Wire, a2: Wire, output: Wire) {
    def andAction() {
      val a1Sig = a1.getSignal
      val a2Sig = a2.getSignal
      afterDelay(AndGateDelay) { output.setSignal(a1Sig & a2Sig) }
    }
    a1 addAction andAction
    a2 addAction andAction
  }

}

object Circuit extends CircuitSimulator {
  def andGateExample {
    println(agenda)
    val in1, in2, out = new Wire
    andGate(in1, in2, out)

    println(agenda)
    
    probe("in1", in1)
    probe("in2", in2)
    probe("out", out)
    in1.setSignal(false)
    in2.setSignal(false)
    run

    in1.setSignal(true)
    run

    in2.setSignal(true)
    run
  }

  val InverterDelay = 1
  val AndGateDelay = 3
  val OrGateDelay = 5

  def demuxOneExample {
    val in, c1 = new Wire
    in.setSignal(true)
    c1.setSignal(true)
    
    //control is true
    val out = List.fill(2)(new Wire)
    demux(in, List(c1), out)
    
    println(out.map(_.getSignal).mkString(","))
    
    c1.setSignal(false)
    
    //control is false
    val out2 = List.fill(2)(new Wire)
    demux(in, List(c1), out2)
    println(out2.map(_.getSignal).mkString(","))
  }
  
  def demuxTwoExample {
    val in = new Wire
    in.setSignal(true)
    
    val bools = Seq(true, false)
    for(i <- bools ; j <- bools) yield {
    	val out = List.fill(4)(new Wire)
    	val c = List.fill(2)(new Wire)
    	c(0).setSignal(i)
    	c(1).setSignal(j)
    	demux(in, c, out)
    	println(out.map(_.getSignal).mkString(","))
    }
    
  }
  
  def demuxBaseExample {
    //base
    val in = new Wire
    val control = Nil
    val out = List(new Wire)
    demux(in, Nil, out)
    
    in.setSignal(true)
    
    
    println(out.map(_.getSignal).mkString(","))
    out.foreach(o => probe("o", o))
  }
}

object CircuitMain extends App {
  // You can write tests either here, or better in the test class CircuitSuite.
//  Circuit.andGateExample
  Circuit.demuxBaseExample
}
