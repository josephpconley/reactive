package nodescala

object race {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
    import concurrent._
	import concurrent.duration._
	import ExecutionContext.Implicits.global
	import nodescala._
  import scala.async.Async.{async, await}
	import scala.util.{Try, Success, Failure}
  
  	def race[T](left: Future[T], right: Future[T]): Future[T] = {
		val p = Promise[T]()
		left onComplete { p.tryComplete(_) }
		right onComplete { p.tryComplete(_) }
		p.future
	}                                         //> race: [T](left: scala.concurrent.Future[T], right: scala.concurrent.Future[T
                                                  //| ])scala.concurrent.Future[T]
	
	val left = Future.always(1)               //> left  : scala.concurrent.Future[Int] = scala.concurrent.impl.Promise$Default
                                                  //| Promise@4a8f5f75
	val right = Future.never                  //> right  : scala.concurrent.Future[Nothing] = scala.concurrent.impl.Promise$De
                                                  //| faultPromise@ac42091
	val winner = race(left, right)            //> winner  : scala.concurrent.Future[Int] = scala.concurrent.impl.Promise$Defau
                                                  //| ltPromise@7a75479a
	left.isCompleted                          //> res0: Boolean = true
	right.isCompleted                         //> res1: Boolean = false
	winner.value                              //> res2: Option[scala.util.Try[Int]] = Some(Success(1))
}