object futures {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  import concurrent._
	import concurrent.duration._
	import ExecutionContext.Implicits.global
	import nodescala._
  import scala.async.Async.{async, await}
	import scala.util.{Try, Success, Failure}

val x = future { 1 }                              //> x  : scala.concurrent.Future[Int] = scala.concurrent.impl.Promise$DefaultPro
                                                  //| mise@640eac4a
	val y = promise[Int]                      //> y  : scala.concurrent.Promise[Int] = scala.concurrent.impl.Promise$DefaultPr
                                                  //| omise@24150c2c
	y completeWith x                          //> res0: futures.y.type = scala.concurrent.impl.Promise$DefaultPromise@24150c2c
                                                  //| 
	y.future onSuccess {
	  case x => println(x)
	}

	val joe = future{
		"joe"
		throw new Exception
	}                                         //> 1
                                                  //| joe  : scala.concurrent.Future[Nothing] = scala.concurrent.impl.Promise$Defa
                                                  //| ultPromise@3d3979cc

	//does eventually complete, but can't put at end of thread (needs time in thread to complete?)
	joe onComplete {
		case Success(j) => println(j)
		case Failure(e) => println("Error" + e)
	}

  val list:List[Future[Int]] = List(Future.always(1), Future.always(2), Future.always(3))
                                                  //> Errorjava.lang.Exception
                                                  //| list  : List[scala.concurrent.Future[Int]] = List(scala.concurrent.impl.Prom
                                                  //| ise$DefaultPromise@116bb691, scala.concurrent.impl.Promise$DefaultPromise@36
                                                  //| 9ef286, scala.concurrent.impl.Promise$DefaultPromise@7d44576d)


	val p = Promise[List[Int]]()              //> p  : scala.concurrent.Promise[List[Int]] = scala.concurrent.impl.Promise$Def
                                                  //| aultPromise@11c7f55b
	p.success(Nil)                            //> res1: futures.p.type = scala.concurrent.impl.Promise$DefaultPromise@11c7f55b
                                                  //| 
	val f = p.future                          //> f  : scala.concurrent.Future[List[Int]] = scala.concurrent.impl.Promise$Defa
                                                  //| ultPromise@11c7f55b
	println(f.value)                          //> Some(Success(List()))
	val b = list.foldRight(p.future){ (f, flist) =>
		for {
			x <- f
			xs <- flist
		}	yield x :: xs
	}                                         //> b  : scala.concurrent.Future[List[Int]] = scala.concurrent.impl.Promise$Defa
                                                  //| ultPromise@2810ba97
  Await.result(b, 1 second)                       //> res2: List[Int] = List(1, 2, 3)
  
  val working = Future.run() { ct =>
    Future {
      while (ct.nonCancelled) {
        println("Working")
        blocking { Thread.sleep(20) } // Just so it doesn't go whizzing off the page
     }
      println("done")
    }
  }                                               //> working  : nodescala.Subscription = nodescala.package$CancellationTokenSour
                                                  //| ce$$anon$1@6b6622e6
	Thread.sleep(30)                          //> Working
                                                  //| Working
	working.unsubscribe()
 	Thread.sleep(30)                          //> done
 	
 	Future.userInput("")                      //> res3: scala.concurrent.Future[String] = scala.concurrent.impl.Promise$Defau
                                                  //| ltPromise@2a7704dd
}