object futures {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(60); 
  println("Welcome to the Scala worksheet")
  import concurrent._
	import concurrent.duration._
	import ExecutionContext.Implicits.global
	import nodescala._
  import scala.async.Async.{async, await}
	import scala.util.{Try, Success, Failure};$skip(231); val res$0 = 

	Future.always(5).isCompleted;System.out.println("""res0: Boolean = """ + $show(res$0));$skip(26); val res$1 = 
	Future.never.isCompleted;System.out.println("""res1: Boolean = """ + $show(res$1));$skip(94); 
  
  val list:List[Future[Int]] = List(Future.always(1), Future.always(2), Future.always(3));System.out.println("""list  : List[scala.concurrent.Future[Int]] = """ + $show(list ));$skip(34); 


	val p = Promise[List[Int]]();System.out.println("""p  : scala.concurrent.Promise[List[Int]] = """ + $show(p ));$skip(16); val res$2 = 
	p.success(Nil);System.out.println("""res2: futures.p.type = """ + $show(res$2));$skip(18); 
	val f = p.future;System.out.println("""f  : scala.concurrent.Future[List[Int]] = """ + $show(f ));$skip(102); 
	val b = list.foldRight(p.future){ (f, flist) =>
		for{
			x <- f
			xs <- flist
		}	yield x :: xs
	};System.out.println("""b  : scala.concurrent.Future[List[Int]] = """ + $show(b ));$skip(31); val res$3 = 
	
  Await.result(b, 1 second);System.out.println("""res3: List[Int] = """ + $show(res$3));$skip(48); 
  
  val xs = List(Set(1, 2, 3), Set(1, 2, 3));System.out.println("""xs  : List[scala.collection.immutable.Set[Int]] = """ + $show(xs ));$skip(13); val res$4 = 
  xs.flatten;System.out.println("""res4: List[Int] = """ + $show(res$4));$skip(49); 
  
  val ys = Set(List(1, 2, 3), List(3, 2, 1));System.out.println("""ys  : scala.collection.immutable.Set[List[Int]] = """ + $show(ys ));$skip(13); val res$5 = 
  ys.flatten;System.out.println("""res5: scala.collection.immutable.Set[Int] = """ + $show(res$5));$skip(146); 
  
  val working = Future.run() { ct =>
	  Future {
	    while (ct.nonCancelled) {
	      println("working")
	    }
	    println("done")
	  }
	};System.out.println("""working  : nodescala.Subscription = """ + $show(working ));$skip(78); 
	
	Future.delay(5 seconds) onSuccess {
	  case _ => working.unsubscribe()
	}}
}
