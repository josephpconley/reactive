object test {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val f: PartialFunction[String, String] = { case "ping" => "pong" }
                                                  //> f  : PartialFunction[String,String] = <function1>
  f("ping")                                       //> res0: String = pong
  f.isDefinedAt("abc")                            //> res1: Boolean = false
  
  val g : PartialFunction[List[Int], String] = {
  	case Nil => "one"
  	case x :: y :: rest => "two"
  }                                               //> g  : PartialFunction[List[Int],String] = <function1>
  g.isDefinedAt(List(1,2,3))                      //> res2: Boolean = true
  
  val h : PartialFunction[List[Int], String] = {
  	case Nil => "one"
  	case x :: rest => rest match {
  		case Nil => "two"
  	}
  }                                               //> h  : PartialFunction[List[Int],String] = <function1>
  h.isDefinedAt(List(1,2,3))                      //> res3: Boolean = true
  //isDefinedAt only applies for outermost block
  
  
}