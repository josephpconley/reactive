object collections {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  for{
  	x <- Some(2)
  	y <- Some(5)
  	z <- None
  } yield (x, y, z)                               //> res0: Option[(Int, Int, Nothing)] = None
}