object random {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  val integers = new Generator[Int]{
  	val rand = new java.util.Random
  	def generate = rand.nextInt
  }                                               //> integers  : Generator[Int]{val rand: java.util.Random} = random$$anonfun$mai
                                                  //| n$1$$anon$1@1a2a46d1
  def booleans = for(x <- integers) yield x > 0   //> booleans: => Generator[Boolean]
  println(booleans.generate)                      //> true
  
  def single[T](x: T): Generator[T] = new Generator[T] {
		def generate = x
	}                                         //> single: [T](x: T)Generator[T]
	
	def choose(lo: Int, hi: Int): Generator[Int] = for (x <- integers) yield lo + x % (hi - lo)
                                                  //> choose: (lo: Int, hi: Int)Generator[Int]

	def oneOf[T](xs: T*): Generator[T] = for (idx <- choose(0, xs.length)) yield xs(idx)
                                                  //> oneOf: [T](xs: T*)Generator[T]
  def trees: Generator[Tree] = for {
  	isLeaf <- booleans
  	tree <- if(isLeaf) getLeaf else getInner
  } yield tree                                    //> trees: => Generator[Tree]
  
  def getLeaf = for(x <- integers) yield Leaf(x)  //> getLeaf: => Generator[Leaf]

	def getInner = for {
		s1 <- trees
		s2 <- trees
	} yield Inner(s1, s2)                     //> getInner: => Generator[Inner]
	
	println(trees.generate)                   //> Leaf(1406104251)
}

//Hint: a tree is either a leaf or an inner node
trait Tree {
	//def print = this match {
	//	case Leaf => println(this)
	//	case Inner => println(this)
	//}
}
case class Inner(left: Tree, right: Tree) extends Tree
case class Leaf(x: Int) extends Tree

trait Generator[+T]{
	self =>	//an alias for "this"
	
	def generate: T
	
	def map[S](f: T => S): Generator[S] = new Generator[S]{
		def generate = f(self.generate)
	}
	
	def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S]{
		def generate = f(self.generate).generate
	}
}