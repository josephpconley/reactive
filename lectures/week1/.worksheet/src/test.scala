object test {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(57); 
  println("Welcome to the Scala worksheet");$skip(72); 
  
  val f: PartialFunction[String, String] = { case "ping" => "pong" };System.out.println("""f  : PartialFunction[String,String] = """ + $show(f ));$skip(12); val res$0 = 
  f("ping");System.out.println("""res0: String = """ + $show(res$0));$skip(23); val res$1 = 
  f.isDefinedAt("abc");System.out.println("""res1: Boolean = """ + $show(res$1));$skip(109); 
  
  val g : PartialFunction[List[Int], String] = {
  	case Nil => "one"
  	case x :: y :: rest => "two"
  };System.out.println("""g  : PartialFunction[List[Int],String] = """ + $show(g ));$skip(29); val res$2 = 
  g.isDefinedAt(List(1,2,3));System.out.println("""res2: Boolean = """ + $show(res$2));$skip(138); 
  
  val h : PartialFunction[List[Int], String] = {
  	case Nil => "one"
  	case x :: rest => rest match {
  		case Nil => "two"
  	}
  };System.out.println("""h  : PartialFunction[List[Int],String] = """ + $show(h ));$skip(29); val res$3 = 
  h.isDefinedAt(List(1,2,3));System.out.println("""res3: Boolean = """ + $show(res$3))}
  //isDefinedAt only applies for outermost block
  
  
}
