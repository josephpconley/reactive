object random {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(59); 
  println("Welcome to the Scala worksheet");$skip(110); 
  
  val integers = new Generator[Int]{
  	val rand = new java.util.Random
  	def generate = rand.nextInt
  };System.out.println("""integers  : Generator[Int]{val rand: java.util.Random} = """ + $show(integers ));$skip(48); 
  def booleans = for(x <- integers) yield x > 0;System.out.println("""booleans: => Generator[Boolean]""");$skip(29); 
  println(booleans.generate);$skip(82); 
  
  def single[T](x: T): Generator[T] = new Generator[T] {
		def generate = x
	};System.out.println("""single: [T](x: T)Generator[T]""");$skip(95); 
	
	def choose(lo: Int, hi: Int): Generator[Int] = for (x <- integers) yield lo + x % (hi - lo);System.out.println("""choose: (lo: Int, hi: Int)Generator[Int]""");$skip(87); 

	def oneOf[T](xs: T*): Generator[T] = for (idx <- choose(0, xs.length)) yield xs(idx);System.out.println("""oneOf: [T](xs: T*)Generator[T]""");$skip(118); 
  def trees: Generator[Tree] = for {
  	isLeaf <- booleans
  	tree <- if(isLeaf) getLeaf else getInner
  } yield tree;System.out.println("""trees: => Generator[Tree]""");$skip(52); 
  
  def getLeaf = for(x <- integers) yield Leaf(x);System.out.println("""getLeaf: => Generator[Leaf]""");$skip(74); 

	def getInner = for {
		s1 <- trees
		s2 <- trees
	} yield Inner(s1, s2);System.out.println("""getInner: => Generator[Inner]""");$skip(27); 
	
	println(trees.generate)}
}

//Hint: a tree is either a leaf or an inner node
trait Tree {
	//def print = this match {
	//	case Leaf => println(this)
	//	case Inner => println(this)
	//}
}
case class Inner(left: Tree, right: Tree) extends Tree
case class Leaf(x: Int) extends Tree

trait Generator[+T]{
	self =>	//an alias for "this"
	
	def generate: T
	
	def map[S](f: T => S): Generator[S] = new Generator[S]{
		def generate = f(self.generate)
	}
	
	def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S]{
		def generate = f(self.generate).generate
	}
}
