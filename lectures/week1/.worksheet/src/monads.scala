object monads {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(59); 
  println("Welcome to the Scala worksheet")}
  
}

case class Success[T](x: T) extends Try[T]
case class Failure(ex: Exception) extends Try[Nothing]
object Try {
	def apply[T](expr: => T): Try[T] =
		try Success(expr)
		catch {
			case NonFatal(ex) => Failure(ex)
		}
}

abstract class Try[+T] {
	def flatMap[U](f: T => Try[U]): Try[U] = this match {
		case Success(x) => try f(x) catch { case NonFatal(ex) => Failure(ex) }
		case fail: Failure => fail
	}
	def map[U](f: T => U): Try[U] = this match {
		case Success(x) => Try(f(x))
		case fail: Failure => fail
	}
}
	
	
Is Try with unit = Try a monad?
	
1)Left unit law - BAD

unit(expr) flatMap f = f(expr)
Try(expr) flatMap f
Try(expr) match {
	case Success(expr) => try f(expr) catch { case NonFatal(ex) => Failure(ex) }
	case fail: Failure => fail
}

If f(expr) raises an exception, it's captured by the left side as a Failure.
The right side would simply throw the exception

try Success(x) catch { case NonFatal(ex) => Failure(ex) } match {
	case Success(x) => try f(x) catch { case NonFatal(ex) => Failure(ex) }
	case fail: Failure => fail
}

3)Associativity
m flatMap f flatMap g == m flatMap (x => f(x) flatMap g)



2)Right unit law - GOOD
m flatMap unit == m
m match {
		case Success(x) => try unit(x) catch { case NonFatal(ex) => Failure(ex) }
		case fail: Failure => fail
	}
	
	m match {
		case Success(x) => try Success(expr) catch { case NonFatal(ex) => Failure(ex)	}
		case fail: Failure => fail
	}	=m
