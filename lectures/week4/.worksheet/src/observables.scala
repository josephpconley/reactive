object observables {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(64); 
  println("Welcome to the Scala worksheet");$skip(69); 
  
	"joe" match {
		case Stud(joe) => println(joe + " is a stud")
	}}
}

//custom deconstructor
object Stud {
	def unapply(x: String): Option[String] = if(x == "joe") Some(x) else None
}

trait Mathematics {
  type Number

	  trait PrimeExtractor {
	    def unapply(x: Number): Option[Number]
	  }
	
	  val Prime: PrimeExtractor
}

trait FastIntegerMath extends Mathematics {
  type Number = Int

  object Prime extends PrimeExtractor {
    def unapply(x: Int): Option[Int] = {
      if ((2 to math.sqrt(x).toInt).forall(x % _ != 0)) Some(x) else None
    }
  }
}

object MyPrimeChecker extends FastIntegerMath {
  def main(args: Array[String]): Unit = args(0).toInt match {
    case Prime(x) => println(s"the number $x is indeed prime.")
    case _ => println("not prime")
  }
}
