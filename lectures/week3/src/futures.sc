object futures {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  import concurrent._
	import concurrent.duration._
	import ExecutionContext.Implicits.global
  
  val f = Future.apply("12893571025710")          //> f  : scala.concurrent.Future[String] = scala.concurrent.impl.Promise$Default
                                                  //| Promise@71be645d
  f.isCompleted                                   //> res0: Boolean = true
  
}