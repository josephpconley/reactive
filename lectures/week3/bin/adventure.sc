object adventure {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  import util.{Try, Success, Failure}
  
  def collectCoins(n: Int): Try[Int] = n match {
  	case n if(n > 5) => Success(n)
  	case _ => Failure(new Exception())
  }                                               //> collectCoins: (n: Int)scala.util.Try[Int]
  
  collectCoins(8)                                 //> res0: scala.util.Try[Int] = Success(8)
  collectCoins(0)                                 //> res1: scala.util.Try[Int] = Failure(java.lang.Exception)
}