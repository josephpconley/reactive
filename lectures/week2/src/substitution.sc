object substitution {
  //substitution model is required for a purely functional language

	val x1 = new BankAccount                  //> x1  : BankAccount = BankAccount@382ce255
	val x2 = new BankAccount                  //> x2  : BankAccount = BankAccount@52751a9b
  
  //not the same as
  
  val x = new BankAccount                         //> x  : BankAccount = BankAccount@625e068
  val y = x                                       //> y  : BankAccount = BankAccount@625e068
}