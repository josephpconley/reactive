object confluence {
  //PURE functional programming, shouldn't matter how you rewrite

	def iterate(n: Int, f: Int => Int, x: Int) : Int = if (n == 0) x else iterate(n-1, f, f(x))
                                                  //> iterate: (n: Int, f: Int => Int, x: Int)Int
 	def square(x: Int) = x * x                //> square: (x: Int)Int
 	
 	iterate(1, square, 3)                     //> res0: Int = 9
 	
 	//holds both ways
 	iterate(0, square, square(3))             //> res1: Int = 9
 	if(1 == 0) 3 else iterate(1 - 1, square, 3 * 3)
                                                  //> res2: Int = 9
   
}