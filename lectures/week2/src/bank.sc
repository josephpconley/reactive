object bank {
  println("Welcome to the Bank")                  //> Welcome to the Bank
  val acct = new BankAccount                      //> acct  : BankAccount = BankAccount@5158ce4
  acct.deposit(50)
  acct withdraw 20                                //> res0: Int = 30
  acct withdraw 20                                //> res1: Int = 10
  
  for(i <- 0 until 10) yield{
  	val a = new BankAccount
  	a.deposit(50)
  	a
  }                                               //> res2: scala.collection.immutable.IndexedSeq[BankAccount] = Vector(BankAccoun
                                                  //| t@59887d29, BankAccount@74ff0f4, BankAccount@115426ec, BankAccount@2bc758d7,
                                                  //|  BankAccount@2c99a1b5, BankAccount@1a4788f3, BankAccount@4e7016ff, BankAccou
                                                  //| nt@20f5e794, BankAccount@7036b673, BankAccount@18047193)
}