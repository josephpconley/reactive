abstract class TestWire{
  def inverter(input: TestWire, output: TestWire): Unit
  def andGate(a1: TestWire, a2: TestWire, output: TestWire): Unit
  def orGate(o1: TestWire, o2: TestWire, output: TestWire): Unit
}

/*
	d = ~ a
	e = ~ b
	f = a & ~ b
	g = b & ~ a
	c = (a & ~ b) | (b & ~ a)
		= a != b


	a | b = ~ (~a & ~b)
  
 * 	
 *   
 */
