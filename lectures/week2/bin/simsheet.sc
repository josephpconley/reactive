object simsheet {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  object sim extends Circuits with Parameters
  import sim._
  
  val in1, in2, sum, carry = new Wire             //> in1  : simsheet.sim.Wire = Gates$Wire@57a220c2
                                                  //| in2  : simsheet.sim.Wire = Gates$Wire@5514cd80
                                                  //| sum  : simsheet.sim.Wire = Gates$Wire@48bfba23
                                                  //| carry  : simsheet.sim.Wire = Gates$Wire@23557525
  
  halfAdder(in1, in2, sum, carry)
  
  probe("sum", sum)                               //> sum 0 new-value = false
  probe("carry", carry)                           //> carry 0 new-value = false
  
  in1 setSignal true
  run()                                           //> *** simulation started, time = 0 ***
  in2 setSignal true
  run()                                           //> *** simulation started, time = 5 ***
                                                  //| carry 8 new-value = true
  
  in1 setSignal false
  run()                                           //> *** simulation started, time = 13 ***
                                                  //| carry 16 new-value = false
}