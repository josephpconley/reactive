object loops {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def WHILE(condition: => Boolean)(command: => Unit): Unit = {
		if (condition) {
			command
			WHILE(condition)(command)
		}
		else ()
  }                                               //> WHILE: (condition: => Boolean)(command: => Unit)Unit

	var x = 5                                 //> x  : Int = 5
	WHILE(x > 0){
		x = x - 1
		println(x)
	}                                         //> 4
                                                  //| 3
                                                  //| 2
                                                  //| 1
                                                  //| 0
		
	def REPEAT(command: => Unit)(condition: => Boolean): Unit = {
		command
		if(condition)() else REPEAT(command)(condition)
	}                                         //> REPEAT: (command: => Unit)(condition: => Boolean)Unit

	var y = 5                                 //> y  : Int = 5
	REPEAT{
		y = y - 1
		println(y)
	}(y < 0)                                  //> 4
                                                  //| 3
                                                  //| 2
                                                  //| 1
                                                  //| 0
                                                  //| -1
  for(i <- 1 until 3; j <- "abc") println(i + " " + j)
                                                  //> 1 a
                                                  //| 1 b
                                                  //| 1 c
                                                  //| 2 a
                                                  //| 2 b
                                                  //| 2 c
}

/*
TODO Exercise - implement in Scala
		REPEAT {
			command
		} UNTIL ( condition )

*/